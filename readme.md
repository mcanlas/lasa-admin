## LASA Documentation:
Lasa-admin app is a backend project which connects to the lasa database and has a user friendly interface to input content for the LASA magazine.

Several images will be referenced which is under the lasa-admin/references.

## Navigating lasa-admin:
Open LASANavExample.png under the references folder. This image shows how the components link to each other and which files are necessary to connect to the database tables and render the views. In the image, the folders/files are color coded and descriptions of each describe how everything is linked. 

When creating a new or editing a view you'll want to add/edit the categories which are in the image. Pay particular attention to public/list.js files. The other folders should be straight forward (css files under css/images under images).

Please reference lasaTables.png to see all the current tables in the database. Some of these tables are connected to the lasa-admin application but there are more tables that still need to be added in(Reference lasaDBtables.png).

## Bugs, Issues and Restructuring

Restructuring:
Consider restructuring the entire app for efficiency. Go through the code, especially under public/js/list.js, the code can be made leaner and more modular. For an example of leaner code please reference bulkytolean.png. This image shows two different variations of code but do the same thing. Things that are repeated a lot can be re-written to be more efficient. In the example image the onChange function and the append function was simplified to only a few lines instead of taking unecessary space.

Modularity:
Please reference exampleModular.png. This code exists in all the list.js files and is an example of what can be made seperate from the parent file. Making code which exsits in multiple file into one global file is ideal for making the app more flexible(Which would decrease memory used and increase page speed). Consider making major changes to the app before moving onto bugs/issues. A good example of making things more modular is the source code for Degrees Abroad CIS(Look under Countries and Main/Global)

Specific Bugs/Issues:
Random Modal appears when searching/refreshing or submitting new data entry in the views. When loggin in, then going to the sign in page navigation bar appears on that page. 
Image uploader button needs overlay styling.(Also needs added features).
In Pages/list.js the edit modal is not functional.
In Advertisements, the adding function is broken(missing values).

##Features that need to be added
Home page could be a tutorial page for how to use lasa-amin (http://bootstraptour.com/
). If 3rd parties want to use the LASA magazine they could input their own content/images in specified areas of the magazine. 
Make all the components WYSIWYG to aviod user confusion. An example would be removing '-' that are in-between sentences so that they equal 'spaces'. 
Seperate Image uploader for each of the catergories(Modularity). The image uploader could include activation function, an image croppper, page preview and drag/drop mode. 
As well as a magazine preview which would show what the digital magazine would look like. Please open SeperateImageUploader.png to reference the concept of seperate image components in each category. 


