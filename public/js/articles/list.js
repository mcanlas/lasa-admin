/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var SearchBar = React.createClass ({
	getInitalState: function() {
		return {
			filterText: ''
		}
	},
	handleChange: function(e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function () {
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} type="search" className="form-control  input-lg" placeholder="Search Title Name" />
				</div>
			</form>
		);
	}
});

var AddArticle = React.createClass ({
	getInitialState: function(){
		return {name: '', content: '', active: 0, color: 'blue', order: ''}
	},
	componentDidMount:function(){
		$(this.refs.textarea.getDOMNode()).redactor({
			changeCallback: this.onChangeContent
		});
	},
	onHandleChange: function(val,e){
		var state = this.state;
	 	state[val] = e.target.value;
	 	this.setState(state);
	},
	onChangeContent:function(html){
		this.setState({content:html})
	},
	upload: function (formData){
		$.ajax({
			url: "/admin/articles/api/add-article",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data){
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit:function(e){
		e.preventDefault();
		if(this.state.name=='' || this.state.content=='')
		{
			alert("You're killing me Smalls. Enter a name and some content!");
			return;
		}
		
		var formData = new FormData();

		formData.append('name', this.state.name)
		formData.append('content', this.state.content)
		formData.append('active', this.state.active)
		formData.append('color', this.state.color)
		formData.append('order', this.state.order)

		this.upload(formData);

		this.setState({name: '', content: '', active: '', color: '', order: ''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide');
		$(this.refs.textarea.getDOMNode()).redactor('destroy');
	},
	render: function () {
		return (
				<div className="modal modal-wide" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
				  <div className="modal-dialog ezCustTrans">
				    <div className="modal-content">
				      <div className="modal-header">
				       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
				       <input type="text" className="form-control" onChange={this.onHandleChange.bind(this, 'name')}  value={this.state.name} placeholder="Title"/>< br/>
				      </div>
				      <div className="modal-body-wide">
				      	<textarea onChange={this.onChangeContent} value={this.state.content}  className="form-control" ref="textarea" />
	        			<form className="form-inline pull-left" role="form">
							<div className="form-group"> Active:
								<select onChange={this.onHandleChange.bind(this, 'active')} value={this.state.active} className="form-control input-lg">
								  <option value="0">not active</option>
								  <option value="1">active</option>
								</select>
							</div>
						</form>
						<form className="form-inline pull-left" role="form">
							<div className="form-group"> Color:
								<select onChange={this.onHandleChange.bind(this, 'color')} value={this.state.color} className="form-control input-lg">
								  <option value="lasa-blue">blue</option>
								  <option value="lasa-green">green</option>
								  <option value="lasa-purple">purple</option>
								  <option value="lasa-red">red</option>
								</select>
							</div>
						</form>
						<form className="form-inline pull-left" role="form">
							<div className="form-group">
						  Order: <input type="number" className="form-control input-lg" name="quantity" onChange={this.onHandleChange.bind(this, 'order')} value={this.state.order}    min="1" max="50"/>
						  	</div>
						</form>
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
			);
	}
});


var EditArticle = React.createClass({
	componentDidUpdate: function() {
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		}else {
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentDidMount:function(){
		$(this.refs.textarea.getDOMNode()).redactor({
			changeCallback: this.onEditContent
		});
	},
	componentWillMount: function(){
		this.setState({name:this.props.data.name, content: this.props.data.content, active: this.props.data.active, color: this.props.data.color, order: this.props.data.order });
	},
	onHandleEdit: function(val,e){
		var state = this.state;
	 	state[val] = e.target.value;
	 	this.setState(state);
	},
	onEditContent: function(html){
		this.setState({content:html})
	},
	handleSave: function(e) {
		if(this.props.name=='' || this.props.content=='')
		{
			alert("You're killing me Smalls. Enter a name at least!");
			return;
		}
		this.props.update({id:this.props.data.id, name:this.state.name, content: this.state.content, active: this.state.active, color: this.state.color, order: this.state.order});
	},
	render: function(){
		return(
				<div className="modal modal-wide" id={"editModal"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
				  <div className="modal-dialog">
				    <div className="modal-content ezCustTrans">
				      <div className="modal-header">
				       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
				       <input type="text"  defaultValue={this.state.name} className="form-control" onChange={this.onHandleEdit.bind(this, 'name')} placeholder="Name"/>< br/>
				      </div>
				      <div className="modal-body-wide">
				      	<textarea defaultValue={this.state.content} onChange={this.onEditContent} className="form-control" ref="textarea" />	
	        			<form className="form-inline pull-left" role="form">
							<div className="form-group">
								<select defaultValue={this.state.active} onChange={this.onHandleEdit.bind(this, 'active')}className="form-control input-lg">
								  <option value="0">not active</option>
								  <option value="1">active</option>
								</select>
							</div>
						</form>	        			

						<form className="form-inline pull-left" role="form">
							<div className="form-group">
								<select defaultValue={this.state.color} onChange={this.onHandleEdit.bind(this, 'color')} className="form-control input-lg">
								  <option value="lasa-blue">blue</option>
								  <option value="lasa-green">green</option>
								  <option value="lasa-purple">purple</option>
								  <option value="lasa-red">red</option>
								</select>
							</div>
						</form>
						<form className="form-inline pull-left" role="form">
							<div className="form-group">
						  Order: <input type="number" className="form-control input-lg" name="quantity" defaultValue={this.state.order} onChange={this.onHandleEdit.bind(this, 'order')} min="1" max="50"/>
						  	</div>
						</form>
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
			);
	}
});

var ArticlesList = React.createClass ({
	getInitialState: function(){
		return {
			editArticlesList: false
		}
	},
	delete: function() {
		if(confirm("Are you sure you want to delete: "+this.props.data.name + "?" +  "There's no going back!") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	update: function(data){
		this.setState({editArticlesList:false});
		this.props.update(data);
	},
	toggleEditArticlesList: function(state) {
		this.setState({editArticle: state || !this.state.editArticle})
	},
	render: function() {
		return (
		<div className="panel-group" id={"accordion"+this.props.data.id}>
		  <div className="panel panel-default">
		    <div className="panel-heading">
		      <h4 className="panel-title">
      		    <a data-toggle="collapse" data-placement="right" data-parent={"#accordion"+this.props.data.id} href={"#collapse"+this.props.data.id}>
				ID: {this.props.data.id} {this.props.data.name}</a>
		      </h4>
		      <div className="article-buttons">
		      <button className="btn btn-primary btn-sm" onClick={this.toggleEditArticlesList}><i className="fa fa-pencil"></i></button>
		      <button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button>
		      </div>
		    </div>
		    <div id={"collapse"+this.props.data.id} className={"panel-collapse collapse in"+this.props.data.id}>
		      <div id="redactor_content" dir="ltr" className="panel-body">
		     {this.props.data.content}
		      </div>
		    </div>
		  </div> 
		  <EditArticle data={this.props.data} update={this.update} state={this.state.editArticle} />
		</div>
			);
	}
});

var ArticlesContainer = React.createClass ({
	getInitialState: function(){
		return {
			data:[],
			filterText: '',
			loading: false,
			refresh:false,
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function() {
		this.refresh();
	},
	refresh: function() {
		$.ajax({
		  url: "/admin/articles/api/get-articles",
		  type: 'POST',
		  beforeSend:function(){
		  	this.setState({refresh:true,loading:true})
		  }.bind(this),
		  success: function(data) {
		  	this.setState({data:data,loading:false})
		  }.bind(this),
		  complete: function(){
		  	this.setState({refresh:false,loading:false})
		  }.bind(this)
		});
	},
	update: function(obj) {
		$.ajax({
	  	  url: "/admin/articles/api/save-article",
	  		data: {
	  			id:obj.id,
	  			name:obj.name,
	  			content:obj.content,
	  			active:obj.active,
	  			color:obj.color,
	  			order:obj.order
	  		},
	  		type: 'POST',
	  		success: function(data) {
	  			this.setState({data:data})
	  		}.bind(this)
		});
	},
	delete: function(id) {
		$.ajax({
		  url: "/admin/articles/api/delete-article",
		  data: {
		  	id:id
		  },
		  type: 'POST',
		  success: function(data) {
		  	this.setState({data:data}) 
		  	}.bind(this)
		});
	},
	filterText: function(filterText) {
		this.setState({
			filterText: filterText
		});
	},
	render: function() {
		var loading = '';
		var filterText = this.state.filterText;
		var rows = [];
		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}

		var filtered = data.filter(function(data){
			return(data.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1) ? true : false;
		});

		filtered.forEach(function(data,index){
			rows.push(<ArticlesList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

		var spinner = cx({
			"fa fa-refresh": true,
			"fa-spin": !this.state.loaded
		})
		return (
			<div>
				<div className="section-header">
					<h2 className="pull-left">Articles</h2>
					<div className={'pull-right right-bar '}>
						<SearchBar onUserInput={this.filterText} />
						<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
						<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>
						<AddArticle refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
					</div>
				</div>
				{loading}
				<div className="container-fluid">
					{rows}
				</div>
				<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i className="fa fa-chevron-up"></i></a>
			</div>
			);
	}
});
	React.renderComponent(<ArticlesContainer />, document.getElementById('content'));