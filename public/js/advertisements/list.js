/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
		);
	}
});

var SearchBar = React.createClass ({
	getInitialState: function() {
		return {
			filterText: ''
		}
	},
	handleChange: function (e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function () {
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} 
					type="search" className="form-control  input-lg" placeholder="Search" />
				</div>
			</form>
		);
	}
});

var AddAdvertisement = React.createClass ({
	getInitalState: function () {
		return {color: 'lasa-red', type: 'two-pane', content:'', left:'', link:'', class:'', logo:''}
	},
	onHandleChange: function(val,e){
		var state = this.state;
		state[val] = e.target.value;
		this.setState(state);
	},
	upload: function(formData){
		$.ajax({
			url: "/admin/advertisements/api/add-advertisement",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit: function(e){
		e.preventDefault();
		var formData = new FormData();
		formData.append('color', this.state.color)
		formData.append('type', this.state.type)
		formData.append('advert_content', this.state.advert_content)
		formData.append('advert_left', this.state.advert_left)
		formData.append('link', this.state.link)
		formData.append('class', this.state.class)
		formData.append('logo', this.state.logo)

		this.upload(formData);

		this.setState({color:'',type:'',advert_content:'',left:'',link:'',class:'',logo:''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide')
	},
	render: function() {
		return (
			<div className="modal modal-wide" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog ezCustTrans">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			       <h3>Advertisement Info</h3>
			      </div>
			     	<div className="modal-body">
				      	<form className="form-inline pull-left" role="form">
							<div className="form-group">
					      	  	<select className="form-control input-lg" onChange={this.onHandleChange.bind(this, 'color')} >
								    <option>lasa-red</option>
								    <option>lasa-green</option>
								    <option>lasa-blue</option>
								    <option>lasa-purple</option>
								</select>
							</div>
						</form><br />
			    		<form className="form-inline pull-left" role="form">
							<div className="form-group">
					      	  	<select className="form-control input-lg" onChange={this.onHandleChange.bind(this, 'type')} >
								    <option>two-pane</option>
								    <option>full</option>
								</select>
							</div>
						</form><br />
							<h6>Link:</h6>
							<input type="text" onChange={this.onHandleChange.bind(this, 'link')}  className="form-control"/>
							<h6>Class:</h6>
							<input type="text" onChange={this.onHandleChange.bind(this, 'class')} className="form-control"/>
							<h6>Advert Content:</h6>
							<textarea onChange={this.onHandleChange.bind(this, 'advert_content')} className="form-control"/>
							<h6>Advert Left:</h6>
							<textarea onChange={this.onHandleChange.bind(this, 'advert_left')}  className="form-control"/>
							<h6>Logo:</h6>
							<input type="text" onChange={this.onHandleChange.bind(this, 'logo')}  className="form-control"/>
			      	</div>
			     	<div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Save changes</button>
				    </div>
			    </div>
			  </div>
			</div>
		);
	}
});


var EditAdvertisement = React.createClass ({
	componentDidUpdate: function(){
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		} else {
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentWillMount: function(){
		this.setState({color:this.props.data.color, type: this.props.data.type, advert_content: this.props.data.advert_content, link: this.props.data.link, class: this.props.data.class, logo: this.props.data.logo});
	},
	handleEdit: function(e){
		var state = this.state;
		state[val] = e.target.value;
		this.setState(state);
	},
	handleSave: function(e){
		this.props.update({id:this.props.data.id, color: this.state.color, type: this.state.type, advert_content: this.state.advert_content, link: this.state.link, class: this.state.class, logo: this.state.logo});
	},
	render: function(){
		return(
			<div className="modal" id={"editModal"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog ezCustTrans">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			       <h3>Advertisement Info</h3>
			      </div>
			      <div className="modal-body">
   			     	<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  	<select className="form-control input-lg" onChange={this.handleEdit.bind(this, 'color')}>
							    <option>lasa-red</option>
							    <option>lasa-green</option>
							    <option>lasa-blue</option>
							    <option>lasa-purple</option>
							</select>
						</div>
					</form><br />
		    		<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  	<select className="form-control input-lg" onChange={this.handleEdit.bind(this, 'type')} >
							    <option>two-pane</option>
							    <option>full</option>
							</select>
						</div>
					</form><br />
						<h6>Link:</h6>
						<input type="text" onChange={this.handleEdit.bind(this, 'link')} className="form-control"/>
						<h6>Class:</h6>
						<input type="text"  onChange={this.handleEdit.bind(this, 'class')}  className="form-control"/>
						<h6>Advert Content:</h6>
						<textarea onChange={this.handleEdit.bind(this, 'advert_content')} className="form-control"/>
						<h6>Advert Left:</h6>
						<textarea onChange={this.handleEdit.bind(this, 'advert_left')} className="form-control"/>
						<h6>Logo:</h6>
						<input type="text"  onChange={this.handleEdit.bind(this, 'logo')}  className="form-control"/>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
		);
	}
});

var AdvertisementList = React.createClass ({
	delete: function() {
		if(confirm("Are you sure you want to delete: " + this.props.data.link + "?" + "There's no going back! ") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	getInitialState: function(){
		return {
			editAdvertisementsList:false
		}
	},
	update: function(data){
		this.setState({editAdvertisementsList:false});
		this.props.update(data);
	},
	toggleEditAdvertisementsList: function(state){
		this.setState({editAdvertisement: state || !this.state.editAdvertisement})
	},
	render: function() {
		return (
				<tr>
					<td>{this.props.data.id}</td>
					<td>{this.props.data.type}</td>
					<td>{this.props.data.color}</td>
					<td>{this.props.data.content}</td>
					<td><a target="_blank" href={this.props.data.link}>{this.props.data.link}</a></td>
					<td>{this.props.data.class}</td>
					<td>{this.props.data.logo}</td>
					<td><button className="btn btn-primary btn-sm" onClick={this.toggleEditAdvertisementsList}><i className="fa fa-pencil"></i></button><EditAdvertisement data={this.props.data} update={this.update} state={this.state.editAdvertisement} /></td>
				  	<td><button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button></td>
				</tr>
			);
	}
});

var AdvertisementContainer = React.createClass({
	getInitialState: function(){
		return {
			data:[],
			filterText:'',
			loading: false,
			refresh:false,
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function() {
		this.refresh();
	},
	refresh: function() {
		$.ajax({
			url: "/admin/advertisements/api/get-advertisements",
			type: 'POST',
			beforeSend:function(){
				this.setState({refresh:true, loading:true})
			}.bind(this),
			success: function(data) {
				this.setState({data:data,loading:false})
			}.bind(this),
			complete: function(){
				this.setState({refresh:false,loading:false})
			}.bind(this)
		});
	},
	update: function(obj) {
		$.ajax({
			url: "/admin/advertisements/api/save-advertisement",
			data: {
				id:obj.id,
				type:obj.type,
				color:obj.color,
				advert_content:obj.advert_content,
				advert_left:obj.advert_left,
				link:obj.link,
				class:obj.class,
				logo:obj.logo
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	delete: function(id){
		$.ajax({
			url: "/admin/advertisements/api/delete-advertisement",
			data: {
				id:id
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this) 
		});
	},
	filterText: function(filterText){
		this.setState({
			filterText: filterText
		});
	},
  	render: function() {
  		var loading = '';
  		var filterText = this.state.filterText;
  		var rows = [];
  		var data = this.state.data;

  		if(this.state.loading)
  			{
  				loading = <Loading />;
  			}

		var filtered = data.filter(function(data){
			return(data.link.toLowerCase().indexOf(filterText.toLowerCase()) > -1) ? true : false;
		});

		filtered.forEach(function(data,index){
			rows.push(<AdvertisementList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

		var spinner = cx({
			"fa fa-refresh": true,
			"fa-spin": this.state.refresh
		});
	    return ( 
	      <div>
	      	<div className="section-header">
	      	<h2 className="pull-left">Advertisement</h2>
	      	<div className={'pull-right right-bar'}>
	      		<SearchBar onUserInput={this.filterText} />
	      		<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
				<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>

				<AddAdvertisement refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
			</div>
			</div>
			{loading}
			<div className="row">
				<div className="col-md-12">
	        		<div className={"table-responsive"}>
	        			<table id="mytable" className="table table-hover table-striped">
							<thead>
							<tr>
							<th>ID</th>
							<th>Type</th>
							<th>Color</th>
							<th>Content</th>
							<th>Link</th>
							<th>Class</th>
							<th>Logo</th>
							</tr>
							</thead>
							<tbody>
							{rows}
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i className="fa fa-chevron-up"></i></a>
	      </div>
      );
  }
});

React.renderComponent(<AdvertisementContainer />, document.getElementById('content'));