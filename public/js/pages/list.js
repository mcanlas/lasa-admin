/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var SearchBar = React.createClass ({
	getInitialState: function() {
		return {
			filterText: ''
		}
	},
	handleChange: function (e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function () {
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} 
					type="search" className="form-control  input-lg" placeholder="Search Alias" />
				</div>
			</form>
			);
	}
});

var Filter = React.createClass({
	getInitalState: function(){
		return {
			value:''
		}
	},
	handleChange: function(e) {
		this.props.filter(e.target.value);
		this.setState({value:e.target.value});
	},
	render: function(){
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<select onChange={this.handleChange} className="form-control input-lg">
					  <option value="">All Types</option>
					  <option value="Advert">Advert</option>
					  <option value="Article">Article</option>
					  <option value="Cover">Cover</option>
					  <option value="Quiz">Quiz</option>
					  <option value="Section">Section</option>
					  <option value="Toc">TOC</option>
					  <option value="Video">Video</option>
					</select>
				</div>
			</form>
			);
	}
});

var AddPage = React.createClass ({
	getInitialState: function () {
		return {alias: '', name: '', color:'lasa-red', type:'Article', ref:'', magazine:'', order: ''}
	},
	onHandleChange: function(val,e){
		var state = this.state;
	 	state[val] = e.target.value;
	 	this.setState(state);
	},
	upload: function(formData){
		$.ajax({
			url: "/admin/pages/api/add-page",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit: function(e) {
		e.preventDefault();
		if(this.state.alias=='' || this.state.name=='')
		{
			alert("Please enter an alias and a name or else a puppy will cry!");
			return;
		}

		var formData = new FormData();
		for(var k in this.state){
			formData.append( k, this.state[k]);
		}
		this.upload(formData);

		this.setState({alias:'', name: '', color:'', type:'', ref:'', magazine:'', order: ''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide')
	},
	render: function() {
		return (
			<div className="modal modal-wide" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog ezCustTrans">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			       <h3>Page Info</h3>
			      </div>
			      <div className="modal-body">
			        <textarea name="alias" rows="1" value={this.state.alias} onChange={this.onHandleChange.bind(this, 'alias')} className="form-control" placeholder="Alias goes here!"  /> <br />
			        <textarea name="name" rows="5" value={this.state.name} onChange={this.onHandleChange.bind(this, 'name')} className="form-control" placeholder="Name goes here!"  /> <br />
			        <form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Order: <input type="number" className="form-control input-lg" name="quantity" value={this.state.order} onChange={this.onHandleChange.bind(this, 'order')} min="1" max="50"/>
					  	</div>
					</form>
					<form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Ref: <input type="number" className="form-control input-lg" name="quantity" value={this.state.ref} onChange={this.onHandleChange.bind(this, 'ref')} min="1" max="50"/>
					  	</div>
					</form>					
					<form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Magazine #: <input type="number" className="form-control input-lg" name="quantity" value={this.state.magazine} onChange={this.onHandleChange.bind(this, 'magazine')} min="1" max="12"/>
					  	</div>
					</form>
    				<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  <select className="form-control input-lg" value={this.state.color} onChange={this.onHandleChange.bind(this, 'color')}>
						    <option value="lasa-blue">blue</option>
						    <option value="lasa-green">green</option>
						    <option value="lasa-purple">purple</option>
						    <option value="lasa-red">red</option>
					  		</select>
					 	 </div>
					</form>
    				<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  <select className="form-control input-lg" value={this.state.type} onChange={this.onHandleChange.bind(this, 'type')}>
						    <option value="Advert">Advert</option>
						    <option value="Article">Article</option>
						    <option value="Cover">Cover</option>
						    <option value="Quiz">Quiz</option>
						    <option value="Section">Section</option>
						    <option value="Toc">TOC</option>
						    <option value="Video">Video</option>
						  </select>
						</div>
					</form>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
			);
	}
});

var EditPage = React.createClass ({
	componenDidUpdate: function(){
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		}else{
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentWillMount: function(){
		this.setState({alias: this.props.data.alias, name: this.props.data.name, type:this.props.data.type, color: this.props.data.color, magazine: this.props.data.magazine, ref: this.props.data.ref, order: this.props.data.order });
	},
	onEditAlias: function(e){
		this.setState({alias:e.target.value})
	},
	onEditName: function(e){
		this.setState({name:e.target.value})
	},
	onEditType: function(e){
		this.setState({type:e.target.value})
	},
	onEditColor: function(e){
		this.setState({color:e.target.value})
	},	
	onEditMagazine: function(e){
		this.setState({magazine:e.target.value})
	},	
	onEditRef: function(e){
		this.setState({ref:e.target.value})
	},
	onEditOrder: function(e){
		this.setState({order:e.target.value})
	},
	handleSave: function(e){
		this.props.update({id:this.props.data.id, alias:this.state.alias, name: this.state.name, type: this.state.type, color: this.state.color, magazine: this.state.magazine, ref: this.state.ref, order: this.state.order});
	},
	render: function(){
		return(
			<div className="modal" id={"editModal"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog ezCustTrans">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			       <h3>Page Info</h3>
			      </div>
			      <div className="modal-body">
			        <textarea name="alias" rows="5" defaultValue={this.state.alias} onChange={this.onEditAlias} className="form-control" /> <br />
			        <textarea name="name" rows="5" defaultValue={this.state.name} onChange={this.onEditName} className="form-control" /> <br />
			        <form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Order: <input type="number" className="form-control input-lg" name="quantity" defaultValue={this.state.order} onChange={this.onEditOrder} min="1" max="50"/>
					  	</div>
					</form>
					<form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Ref: <input type="number" className="form-control input-lg" name="quantity" defaultValue={this.state.ref} onChange={this.onEditRef} min="1" max="50"/>
					  	</div>
					</form>					
					<form className="form-inline pull-left" role="form">
						<div className="form-group">
			        		Magazine #: <input type="number" className="form-control input-lg" name="quantity" defaultValue={this.state.magazine} onChange={this.onEditMagazine} min="1" max="12"/>
					  	</div>
					</form>
    				<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  <select className="form-control input-lg" onChange={this.onEditColor}>
						    <option value="lasa-blue">blue</option>
						    <option value="lasa-green">green</option>
						    <option value="lasa-purple">purple</option>
						    <option value="lasa-red">red</option>
					  		</select>
					 	 </div>
					</form>
    				<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  <select className="form-control input-lg" onChange={this.onEditType}>
						    <option value="Advert">Advert</option>
						    <option value="Article">Article</option>
						    <option value="Cover">Cover</option>
						    <option value="Quiz">Quiz</option>
						    <option value="Section">Section</option>
						    <option value="Toc">TOC</option>
						    <option value="Video">Video</option>
						  </select>
						</div>
					</form>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
		);
	}
}); 

var PagesList = React.createClass ({
	getInitialState: function(){
		return {editPageList: false}
	},
	delete: function() {
		if(confirm("Are you sure you want to delete: " + this.props.data.alias + " ? " + "There's no going back!") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	update: function(data){
		this.setState({editPagesList:false});
		this.props.update(data);
	},
	toggleEditPagesList: function(state){
		this.setState({editPage: state || !this.state.editPage})
	},
	render: function() {
		return (
				<tr>
					<td>{this.props.data.id}</td>
					<td>{this.props.data.type}</td>
					<td>{this.props.data.alias}</td>
					<td>{this.props.data.name}</td>
					<td>{this.props.data.color}</td>
					<td>{this.props.data.ref}</td>
					<td>{this.props.data.order}</td>
					<td>{this.props.data.magazine}</td>
					<td><button className="btn btn-primary btn-sm" onClick={this.toggleEditPagesList}><i className="fa fa-pencil"></i></button><EditPage data={this.props.data} update={this.update} state={this.state.editPage} /></td>
				  	<td><button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button></td>
				</tr>
			);
	}
});


var PagesContainer = React.createClass({
	getInitialState: function() {
		return {
			data:[],
			filterText: '',
			refresh: false,
			loading: false,
			filter:''
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function() {
		this.refresh();
	},
	refresh: function() {
		$.ajax({
			url: "/admin/pages/api/get-pages",
			type: 'POST',
			beforeSend:function(){
				this.setState({refresh:true, loading:true})
			}.bind(this),
			success: function(data) {
				this.setState({data:data,loading:false})
			}.bind(this),
			complete: function(){
				this.setState({refresh:false,loading:false})
			}.bind(this)
		});
	},
	update: function(obj) {
		$.ajax({
			url: "/admin/pages/api/save-page",
			data: {
				id:obj.id,
				type:obj.type,
				color:obj.color,
				alias:obj.alias,
				name:obj.name,
				ref:obj.ref,
				order:obj.order,
				magazine:obj.magazine
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	delete: function(id){
		$.ajax({
			url: "/admin/pages/api/delete-page",
			data: {
				id:id
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this) 
		});
	},
	doFilterText: function(data){
		if(this.state.filterText=='') return true;
		return data.alias.toLowerCase().indexOf(this.state.filterText.toLowerCase()) > -1;
	},
	filterText: function(filterText){
		this.setState({
			filterText: filterText
		});
	},
	filter: function(val){
		this.setState({filter:val});
	},
	doFilter: function(element){
		if(this.state.filter=='') return true;
		return element.type==this.state.filter;
	},
	render: function() {
		var loading = '';
		var filterText = this.state.filterText;
		var rows = [];
		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}

		data.filter(this.doFilterText).filter(this.doFilter).forEach(function(data,index){
			rows.push(<PagesList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

		var spinner = cx({
			"fa fa-refresh": true,
			"fa-spin": this.state.refresh
		});
    	return ( 
	      <div>
	      	<div className="section-header">
	      	<h2 className="pull-left">Pages</h2>
	      	<div className={'pull-right right-bar'}>
	      		<SearchBar onUserInput={this.filterText} />
	      		<Filter filter={this.filter}  />
	      		<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
				<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>

				<AddPage refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
			</div>
			</div>
			{loading}
			<div className="row">
				<div className="col-md-12">
	        		<div className={"table-responsive"}>
	        			<table id="mytable" className="table table-hover table-striped">
						<thead>
							<tr>
							<th>ID</th>
							<th>Type</th>
							<th>Alias</th>
							<th>Name</th>
							<th>Color</th>
							<th>Ref</th>
							<th>Order</th>
							<th>Magazine</th>
							</tr>
						</thead>
					<tbody>
					{rows}
					</tbody>
						</table>
					</div>
				</div>
			</div>
			<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button" data-toggle="tooltip" data-placement="left"><i className="fa fa-chevron-up"></i></a>
	      </div>
      );
  }
});

React.renderComponent(<PagesContainer />, document.getElementById('content'));