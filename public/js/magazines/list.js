/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var MagazineList = React.createClass ({
	getInitialState: function(){
		return {
			editAuthorsList: false
		}
	},
	delete: function() {
		if(confirm("Are you sure you want to delete: "+this.props.data.key + "? "+  "There's no going back!") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	update: function(data){
		this.setState({editAuthorsList:false});
		this.props.update(data);
	},
	toggleEditAuthorsList: function(state) {
		this.setState({editAuthor: state || !this.state.editAuthor})
	},
	render: function() {
		return ( 
				<tr>
					<td>{this.props.data.id}</td>
				    <td>{this.props.data.key}</td>
				    <td><button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button></td>
				</tr>
			);
	}
});

var MagazinesContainer = React.createClass ({
	getInitialState: function(){
		return {
			data:[],
			filterText: '',
			refresh:false,
			loading: false,
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function() {
		this.refresh();
	},
	generate: function(){
		var randomString = Math.random().toString();
		var hash = CryptoJS.MD5(randomString);
		var formData = new FormData();
		formData.append('key', hash);
		this.upload(formData);
	},
	upload: function(formData){
			$.ajax({
			url: "/admin/magazines/api/add-magazine",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function(){
				this.setState({refresh:true, loading:true})
			}.bind(this),
			success: function(data){
				this.setState({data:data, loading:false})
			}.bind(this)
		});
	},
	refresh: function() {
		$.ajax({
		  url: "/admin/magazines/api/get-magazines",
		  type: 'POST',
		  beforeSend:function(){
		  	this.setState({refresh:true,loading:true})
		  }.bind(this),
		  success: function(data) {
		  	this.setState({data:data,loading:false})
		  }.bind(this),
		  complete: function(){
		  	this.setState({refresh:false,loading:false})
		  }.bind(this)
		});
	},
	delete: function(id) {
		$.ajax({
		  url: "/admin/magazines/api/delete-magazine",
		  data: {
		  	id:id
		  },
		  type: 'POST',
		  success: function(data) {
		  	this.setState({data:data})
		  	}.bind(this)
		});
	},
	render: function() {
		var loading = '';
		var filterText = this.state.filterText;
		var rows = [];
		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}

		var filtered = data.filter(function(data){
			return(data.key.toLowerCase().indexOf(filterText.toLowerCase()) > -1) ? true : false;
		});

		filtered.forEach(function(data,index){
			rows.push(<MagazineList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

		return (
			<div>
				<div className="section-header">
					<h2 className="pull-left">Magazine</h2>
					<div className={'pull-right right-bar '}>
						<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
						<button className="btn btn-primary btn-lg" onClick={this.generate}>Generate New Magazine <i className="fa fa-plus"></i></button>
					</div>
				</div>
				{loading}
				
					<div className="row">
						<div className="col-md-12">
			        		<div className={"table-responsive"}>
			        			<table id="mytable" className="table table-hover table-striped">
			        			<thead>
    					            <th>Id</th>
					                <th>Key</th>
			        			</thead>
			        			<tbody>
			        			{rows}
			        			</tbody>
								</table>
							</div>
						</div>
					</div>
				<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button" data-toggle="tooltip" data-placement="left"><i className="fa fa-chevron-up"></i></a>
			</div>
			);
	}
});
	React.renderComponent(<MagazinesContainer />, document.getElementById('content'));