/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var Filter = React.createClass({
	getInitalState: function(){
		return {
			value:''
		}
	},
	handleChange: function(e) {
		this.props.filter(e.target.value);
		this.setState({value:e.target.value});
	},
	render: function(){
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<select onChange={this.handleChange} className="form-control input-lg">
					  <option value="">All Types</option>
					  <option value="advertisements">advertisements</option>
					  <option value="articles">articles</option>
					  <option value="authors">authors</option>
					  <option value="magazine">magazine</option>
					  <option value="quiz">quiz</option>
					  <option value="TOC">TOC</option>
					  <option value="videos">videos</option>
					</select>
				</div>
			</form>
			);
	}
});

var SearchBar = React.createClass ({
	getInitialState: function() {
		return {
			filterText:''
		}
	},
	handleChange: function (e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function (){
		return (
			<form className="form-inline pull-left" fole="form">
				<div clasName="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} type="search" className="form-control  input-lg" placeholder="Search" />
				</div>
			</form>
			);
	}
});


var AddImage = React.createClass ({
	getInitialState: function(){
		return { position_x: 'center', position_y: 'center', type: 'magazine'}
	},
	onChangeX:function(e){
		this.setState({position_x:e.target.value})
	},	
	onChangeY:function(e){
		this.setState({position_y:e.target.value})
	},
	onChangeType: function(e){
		this.setState({type:e.target.value})
	},
	upload: function (formData){
		$.ajax({
			url: "/admin/images/api/add-image",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data){
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit:function(e){
		e.preventDefault();
		
		var formData = new FormData();
		var files = this.refs.upload.getDOMNode().files;		

		formData.append('url', files[0]);
		formData.append('position_x', this.state.position_x)
		formData.append('position_y', this.state.position_y)
		formData.append('type', this.state.type)

		this.upload(formData);

		this.setState({ position_x: '', position_y: '', type: ''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide')
	},
	render: function () {
		return (
			<div className="modal fade" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
						<div className="file-input-wrapper">
							<button id="img-upload-button" type="button" className={'btn btn-primary'}>Upload an Image</button>
							<input type="file" name="image" ref="upload" id="testiImg" accept="image/*" method="post" />
						</div>
			      </div>
			      <div className="modal-body">
	      	  		<form className="form-inline pull-left " role="form">
						<div className="form-group">
				      	  	<select className="form-control input-lg" value={this.state.type} onChange={this.onChangeType}>
							    <option value="advertisements">advertisements</option>
							    <option value="articles">articles</option>
							    <option value="authors">authors</option>
							    <option value="cover">cover</option>
							    <option value="main">main</option>
							    <option value="quiz">quiz</option>
							    <option value="TOC">TOC</option>
							    <option value="videos">videos</option>
							    <option value="magazine">magazine</option>
							</select>
						</div>
					</form>
		      	  	<form className="form-inline pull-left" role="form">
						<div className="form-group">
				      	  	<select className="form-control input-lg" value={this.state.position_x} onChange={this.onChangeX}>
							    <option value="center">center</option>
							    <option value="left">left</option>
							    <option value="right">right</option>
							</select>
						</div>
					</form>
		      	  	<form className="form-inline " role="form">
						<div className="form-group">
				      	  	<select className="form-control input-lg" value={this.state.position_y} onChange={this.onChangeY}>
							    <option value="center">center</option>
							    <option value="left">left</option>
							    <option value="right">right</option>
							</select>
						</div>
					</form>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
			);
	}
});

var EditImage = React.createClass({
	componentDidUpdate: function() {
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		}else {
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentWillMount: function() {
		this.setState({video:this.props.data.video, link:this.props.data.link, title:this.props.data.title, color:this.props.data.color, type:this.props.data.type });
	},
	onEditLink: function(e){
		this.setState({link:e.target.value})
	},
	onEditTitle: function (e) {
		this.setState({title:e.target.value})
	},
	onEditColor: function (e) {
		this.setState({color:e.target.value})
	},
	onEditType: function (e) {
		this.setState({type:e.target.value})
	},
	handleSave: function(e){
		if (this.props.link == '' || this.props.title == '')
		{
			alert('Fill empty fields por favor');
			return;
		}
		thist.props.update({id:this.props.data.id, link:this.state.link, title: this.state.title, type:this.state.type, color:this.state.color});
	},
	render: function(){
		return(
			<div className="modal fade" id={"modalEdit"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog">
			    <div className="modal-content">
			      <div className="modal-header">
			        <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			        <input type="text"  className="form-control" onChange={this.onEditTitle} defaultValue={this.state.title} />
			      </div>
			      <div className="modal-body">
			        <textarea name="bio" rows="10" className="form-control" onChange={this.onEditType} defaultValue={this.state.bio}  />
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
		);
	}
});

var ImageList = React.createClass ({
	getInitialState: function(){
		return {editImagesList: false}
	},
	delete: function() {
		if(confirm("Are you sure you want to delete" + this.props.data.url) == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	update: function(data){
	this.setState({editImagesList:false});
	this.props.update(data);
	},
	toggleEditImagesList: function(state){
		this.setState({editImage: state || !this.state.editImage})
	},
	render: function() {
		return(
			<div>	
				<div className="container-img" >
					<div className="row-img form-group">
				        <div className="col-sm-4 col-xs-6 col-md-3 col-lg-3">
				            <div className="panel panel-default">
				                <div className={"panel-image"}>
				                    <img src={"images/magazine/"+this.props.data.type+"/"+this.props.data.url} className="panel-image-preview" />
				                </div>
				                <div className="panel-footer text-left">
								<h6><strong>{this.props.data.url}</strong></h6>
								<h6>{this.props.data.position_x}</h6>
								<h6>{this.props.data.position_y}</h6>
								<h6>{this.props.data.type}</h6>
				                </div>
				            </div>
				        </div>
			    	</div>
				</div>
				<EditImage data={this.props.data} update={this.update} state={this.state.editImage} />
            </div>
			);
	}
}); 

var ImagesContainer = React.createClass({
	getInitialState: function(){
		return {
			data: [],
			filterText: '',
			refresh: false,
			loading:false,
			filter: ''
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function(){
		this.refresh();
	},
	refresh: function() {
		$.ajax({
			url: "admin/images/api/get-images",
			type: 'POST',
			beforeSend: function() {
				this.setState({refresh:true, loading:true})
			}.bind(this),
			success: function(data){
				this.setState({data:data, loading: false})
			}.bind(this),
			complete: function(){
				this.setState({refresh:false, loading:false})
			}.bind(this)
		});
	},
	update: function(obj){
		$.ajax({
			url: "/admin/images/api/save-image",
			data: {
				id:obj.id,
				url:obj.url,
				position_x:obj.position_x,
				position_y:obj.position_y,
				type:obj.type
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	delete: function(id) {
		$.ajax({
			url: "/admin/images/api/delete-image",
			data: {
				id:id
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	doFilterText: function(data){
		if(this.state.filterText=='') return true;
		return data.url.toLowerCase().indexOf(this.state.filterText.toLowerCase()) > -1;
	},
	filterText: function(filterText){
		this.setState({
			filterText: filterText
		});
	},
	filter: function(val){
		this.setState({filter:val});
	},
	doFilter: function(element){
		if(this.state.filter=='') return true;
		return element.type==this.state.filter;
	},
  	render: function() {
  		var loading = '';
  		var filterText = this.state.filterText;
  		var rows = [];
  		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}
 
		data.filter(this.doFilterText).filter(this.doFilter).forEach(function(data,index){
			rows.push(<ImageList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

  		var spinner = cx({
  			"fa fa-refresh": true,
  			"fa-spin": this.state.refresh
  		})
    return ( 
      <div>
      	<div className="section-header">
      	<h2 className="pull-left">Images</h2>
      	<div className={'pull-right right-bar'}>
      		<SearchBar onUserInput={this.filterText} />
      		<Filter filter={this.filter}  />
      		<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
			<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>
			<AddImage refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
		</div>
		</div>
		{loading}
		<div className='list-group gallery'>
			{rows}
		</div>
		<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i className="fa fa-chevron-up"></i></a>
	  </div>	
      );
  }
});

React.renderComponent(<ImagesContainer />, document.getElementById('content'));