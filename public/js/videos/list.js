/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var SearchBar = React.createClass ({
	getInitialState: function() {
		return {
			filterText:''
		}
	},
	handleChange: function (e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function (){
		return (
			<form className="form-inline pull-left" fole="form">
				<div clasName="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} type="search" className="form-control  input-lg" placeholder="Search" />
				</div>
			</form>
			);
	}
});

var Filter = React.createClass({
	getInitalState: function(){
		return {
			value:''
		}
	},
	handleChange: function(e) {
		this.props.filter(e.target.value);
		this.setState({value:e.target.value});
	},
	render: function(){
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<select onChange={this.handleChange} className="form-control input-lg">
					  <option value="">All Videos</option>
					  <option value="alliance">Alliance</option>
					  <option value="bridge">Bridge</option>
					  <option value="lasa">LASA</option>
					  <option value="tefl">TEFL</option>
					</select>
				</div>
			</form>
			);
	}
});

var AddVideo = React.createClass ({
	getInitialState: function(){
		return {color: 'lasa-red', class: 'lasa', video: '', title: '', text: '', link: ''}
	},
	onHandleChange: function(val,e){
		var state = this.state;
	 	state[val] = e.target.value;
	 	this.setState(state);
	},
	upload: function (formData){
		$.ajax({
			url: "/admin/videos/api/add-video",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data){
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit: function(e){
		e.preventDefault();
		if (this.state.video==''){
			alert("Please enter a video link");
			return;
		}

		var formData = new FormData();

		for(var k in this.state){
			formData.append( k, this.state[k]);
		}

		this.upload(formData);

		this.setState({color:'', class: '', video: '', title: '', text: '', link: ''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide')
	},
	render: function(){
		return (
			<div className="modal fade" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
			  <div className="modal-dialog ezCustTrans">
			    <div className="modal-content">
			      <div className="modal-header">
			       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
			       <h3>Add a new video</h3>
			      </div>
			      <div className="modal-body">
		      	  	<form className="form-inline pull-left" role="form">
						<div className="form-group">
						<h6>Color: </h6>
				      	  	<select className="form-control input-lg" onChange={this.onHandleChange.bind(this, 'color')} value={this.state.color}  >
							    <option>lasa-red</option>
							    <option>lasa-green</option>
							    <option>lasa-blue</option>
							    <option>lasa-purple</option>
							</select>
						</div>
					</form>
		      	  	<form className="form-inline pull-left" role="form">
						<div className="form-group">
						<h6>Class: </h6>
				      	  	<select className="form-control input-lg"   onChange={this.onHandleChange.bind(this, 'class')} value={this.state.class}>
							    <option>alliance</option>
							    <option>bridge</option>
							    <option>lasa</option>
							    <option>tefl</option>
							</select>
						</div>
					</form>
					<h6>Title: </h6><input type="text"  onChange={this.onHandleChange.bind(this, 'title')} value={this.state.title} className="form-control"  placeholder="*optional*"/>
			      	<h6>Video Embed Link:</h6><input type="text"  onChange={this.onHandleChange.bind(this, 'video')} value={this.state.video} className="form-control"  /> 
			      	<h6>Text:</h6><input type="text"  onChange={this.onHandleChange.bind(this, 'text')} value={this.state.text} className="form-control"  placeholder="*optional*"/>
			      	<h6>Link:</h6><input type="text"  onChange={this.onHandleChange.bind(this, 'link')} value={this.state.link} className="form-control"  placeholder="*optional*"/>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" onClick={this.handleSubmit} className="btn btn-primary">Save</button>
			      </div>
			    </div>
			  </div>
			</div>
			);
	}
});

var EditVideo = React.createClass({
	componentDidUpdate: function() {
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		}else {
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentWillMount: function() {
		this.setState({video:this.props.data.video, link:this.props.data.link, text:this.props.data.text, title:this.props.data.title, color:this.props.data.color, class:this.props.data.class });
	},
	onEditLink: function(e){
		this.setState({link:e.target.value})
	},
	onEditTitle: function (e) {
		this.setState({title:e.target.value})
	},
	onEditText: function (e) {
		this.setState({text:e.target.value})
	},
	onEditColor: function (e) {
		this.setState({color:e.target.value})
	},
	onEditClass: function (e) {
		this.setState({class:e.target.value})
	},
	onEditVideo: function (e) {
		this.setState({video:e.target.value})
	},
	handleSave: function(e){
		if (this.props.link == '' || this.props.title == '')
		{
			alert('Fill empty fields por favor');
			return;
		}
		this.props.update({id:this.props.data.id, video:this.state.video, link:this.state.link, title: this.state.title, class: this.state.class, color:this.state.color, text:this.state.text});
	},
	render: function(){
		return(
				<div className="modal" id={"editModal"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
				  <div className="modal-dialog ezCustTrans">
				    <div className="modal-content">
				      <div className="modal-header">
				        <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
				      </div>
				      <div className="modal-body">
				      	  	<form className="form-inline pull-left" role="form">
								<div className="form-group">
								<h6>Color: </h6>
						      	  	<select defaultValue={this.state.color} onChange={this.onEditColor} className="form-control input-lg" >
									    <option>lasa-red</option>
									    <option>lasa-green</option>
									    <option>lasa-blue</option>
									    <option>lasa-purple</option>
									</select>
								</div>
							</form><br />
				      	  	<form className="form-inline pull-left" role="form">
								<div className="form-group">
								<h6>Class: </h6>
						      	  	<select defaultValue={this.state.class} onChange={this.onEditClass} className="form-control input-lg"  >
									    <option>alliance</option>
									    <option>bridge</option>
									    <option>lasa</option>
									    <option>tefl</option>
									</select>
								</div>
							</form>
							<h6>Title: </h6><input type="text"  onChange={this.onEditTitle}  className="form-control"  defaultValue={this.state.title}/>
					      	<h6>Video Embed Link:</h6><input type="text" onChange={this.onEditVideo} defaultValue={this.state.video}   className="form-control"  /> 
					      	<h6>Text:</h6><input type="text" onChange={this.onEditText} defaultValue={this.state.text} className="form-control"  />
					      	<h6>Link:</h6><input type="text" onChange={this.onEditLink} defaultValue={this.state.link}  className="form-control" />
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
		);
	}
});

var VideoList = React.createClass ({
	getInitialState: function(){
		return {editVideosList: false}
	},
	delete: function() {
		if(confirm("Are you sure you want to delete this video?") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	update: function(data){
	this.setState({editVideosList:false});
	this.props.update(data);
	},
	toggleEditVideosList: function(state){
		this.setState({editVideo: state || !this.state.editVideo})
	},
	render: function() {
		return(
			<tr>
				<td>{this.props.data.title}</td>
				<td>{this.props.data.text}</td>
				<td>{this.props.data.color}</td> 
				<td>{this.props.data.class}</td> 
				<td><a target="_blank" href={this.props.data.video}>{this.props.data.video}</a></td>
				<td><a target="_blank" href={this.props.data.link}>{this.props.data.link}</a></td>
				<td><button className="btn btn-primary btn-sm" onClick={this.toggleEditVideosList}><i className="fa fa-pencil"></i></button><EditVideo data={this.props.data} update={this.update} state={this.state.editVideo} /></td>
			  	<td><button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button></td>
			</tr>
			);
	}
}); 

var VideosContainer = React.createClass({
	getInitialState: function(){
		return {
			data: [],
			filterText: '',
			refresh: false,
			loading:false,
			filter: '',
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function(){
		this.refresh();
	},
	refresh: function() {
		$.ajax({
			url: "admin/videos/api/get-videos",
			type: 'POST',
			beforeSend: function() {
				this.setState({refresh:true, loading:true})
			}.bind(this),
			success: function(data){
				this.setState({data:data, loading: false})
			}.bind(this),
			complete: function(){
				this.setState({refresh:false, loading:false})
			}.bind(this)
		});
	},
	update: function(obj){
		$.ajax({
			url: "/admin/videos/api/save-video",
			data: {
				id:obj.id,
				video:obj.video,
				title:obj.title,
				text:obj.text,
				link:obj.link,
				color:obj.color,
				type: obj.type
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	delete: function(id) {
		$.ajax({
			url: "/admin/videos/api/delete-video",
			data: {
				id:id
			},
			type: 'POST',
			success: function(data) {
				this.setState({data:data})
			}.bind(this)
		});
	},
	doFilterText: function(data){
		if(this.state.filterText=='') return true;
		return data.alias.toLowerCase().indexOf(this.state.filterText.toLowerCase()) > -1;
	},
	filterText: function(filterText){
		this.setState({
			filterText: filterText
		});
	},
	filter: function(val){
		this.setState({filter:val});
	},
	doFilter: function(element){
		if(this.state.filter=='') return true;
		return element.class==this.state.filter;
	},
  	render: function() {
  		var loading = '';
  		var filterText = this.state.filterText;
  		var rows = [];
  		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}
 
		data.filter(this.doFilterText).filter(this.doFilter).forEach(function(data,index){
			rows.push(<VideoList update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))

  		var spinner = cx({
  			"fa fa-refresh": true,
  			"fa-spin": this.state.refresh
  		})
    return ( 
  	<div>
      	<div className="section-header">
      	<h2 className="pull-left">Videos</h2>
      	<div className={'pull-right right-bar'}>
      		<SearchBar onUserInput={this.filterText} />
      		<Filter filter={this.filter} />
      		<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
			<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>

			<AddVideo refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
		</div>
		</div>
		{loading}
		<div className="row">
			<div className="col-md-12">
        		<div className={"table-responsive"}>
        			<table id="mytable" className="table table-hover table-striped">
					<thead>
			            <th>Title</th>
			            <th>Text</th>
			            <th>Color</th>
			            <th>Class</th>
			            <th>Video</th>
			            <th>Link</th>
					</thead>
					<tbody>
					{rows}
					</tbody>
					</table>
				</div>
			</div>
		</div>
			<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button"><i className="fa fa-chevron-up"></i></a>
	</div>
      );
  }
});

React.renderComponent(<VideosContainer />, document.getElementById('content'));