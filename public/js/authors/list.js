/** @jsx React.DOM */
var cx = React.addons.classSet;

var Loading = React.createClass({
	render: function(){
		return (
			<div className="loading">
				<div>
					<i className="fa fa-circle-o-notch fa-spin"></i>
				</div>
			</div>
			);
	}
});

var SearchBar = React.createClass ({
	getInitalState: function() {
		return {
			filterText: ''
		}
	},
	handleChange: function(e) {
		this.setState({filterText:e.target.value})
		this.props.onUserInput(e.target.value);
	},
	render: function () {
		return (
			<form className="form-inline pull-left" role="form">
				<div className="form-group">
					<input ref="filterTextInput" onChange={this.handleChange} type="search" className="form-control  input-lg" placeholder="Search" />
				</div>
			</form>
			);
	}
});


var AddAuthor = React.createClass ({
	getInitialState: function(){
		return {name: '', bio: '', article: ''}
	},
	onChangeName: function(e){
		this.setState({name:e.target.value})
	},
	onChangeBio:function(e){
		this.setState({bio:e.target.value})
	},	
	onChangeArticle:function(e){
		this.setState({article:e.target.value})
	},
	upload: function (formData){
		$.ajax({
			url: "/admin/authors/api/add-author",
			type: 'POST',
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			success: function(data){
				this.props.softRefresh(data);
			}.bind(this)
		});
	},
	handleSubmit:function(e){
		e.preventDefault();
		if(this.state.name=='' || this.state.bio=='')
		{
			alert("You're killing me Smalls. Enter a name and a bio!");
			return;
		}
		var formData = new FormData();
		formData.append('name', this.state.name)
		formData.append('bio', this.state.bio)
		formData.append('article', this.state.article)

		this.upload(formData);

		this.setState({name:'', bio: '', article: ''})
		$(this.refs.modalAdd.getDOMNode()).modal('hide')
	},
	render: function () {
		return (
				<div className="modal fade" id="modalAdd" ref="modalAdd" tabIndex="-1" role="dialog"  aria-hidden="true">
				  <div className="modal-dialog ezCustTrans">
				    <div className="modal-content">
				      <div className="modal-header">
				       <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
				       <input type="text"  value={this.state.name} className="form-control" onChange={this.onChangeName} placeholder="Name"/>< br/>
				       <input type="text"  value={this.state.article} className="form-control" onChange={this.onChangeArticle} placeholder="Article Reference ID"/>
				      </div>
				      <div className="modal-body">
				        <textarea name="bio" rows="10" value={this.state.bio} onChange={this.onChangeBio} className="form-control" placeholder="Bio goes here!"  />
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
			);
	}
});


var EditAuthor = React.createClass({
	componentDidUpdate: function() {
		if(this.props.state){
			$(this.refs.modalEdit.getDOMNode()).modal('show')
		}else {
			$(this.refs.modalEdit.getDOMNode()).modal('hide')
		}
	},
	componentWillMount: function(){
		this.setState({name:this.props.data.name, bio: this.props.data.bio, article: this.props.data.article});
	},
	onEditName: function(e){
		this.setState({name:e.target.value})
	},
	onEditBio: function(e){
		this.setState({bio:e.target.value})
	},
	onEditArticle: function(e){
		this.setState({article:e.target.value})
	},
	handleSave: function(e) {
		if(this.props.name=='' || this.props.bio=='')
		{
			alert("You're killing me Smalls. Enter a name at least!");
			return;
		}
		this.props.update({id:this.props.data.id, name:this.state.name, bio: this.state.bio, article: this.state.article});
	},
	render: function(){
		return(
				<div className="modal " id={"editModal"+this.props.data.id} ref="modalEdit" tabIndex="-1" role="dialog"  aria-hidden="true">
				  <div className="modal-dialog ezCustTrans">
				    <div className="modal-content">
				      <div className="modal-header">
				        <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
				        <input type="text"  className="form-control" onChange={this.onEditName} defaultValue={this.state.name} />
				        <input type="text"  className="form-control" onChange={this.onEditArticle} defaultValue={this.state.article} />			
				      </div>
				      <div className="modal-body">
				        <textarea name="bio" rows="10" className="form-control" onChange={this.onEditBio} defaultValue={this.state.bio}  />
				      </div>
				      <div className="modal-footer">
				        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" className="btn btn-primary" onClick={this.handleSave}>Save</button>
				      </div>
				    </div>
				  </div>
				</div>
			);
	}
});

var AuthorsContent = React.createClass ({
	delete: function() {
		if(confirm("Are you sure you want to delete: "+this.props.data.name + "?" +  "There's no going back!") == true){
			this.props.delete(this.props.data.id);
		}
		return;
	},
	toggleEditAuthorsList: function(state) {
		this.setState({editAuthor: state || !this.state.editAuthor})
	},
	getInitialState: function(){
		return {
			editAuthorsList: false
		}
	},
	update: function(data){
		this.setState({editAuthorsList:false});
		this.props.update(data);
	},
	render: function(){
		return (
				<tr>
					<td>{this.props.data.id}</td>
				    <td>{this.props.data.name}</td>
				    <td>{this.props.data.bio}</td>
				    <td><button className="btn btn-primary btn-sm" onClick={this.toggleEditAuthorsList}><i className="fa fa-pencil"></i></button><EditAuthor data={this.props.data} update={this.update} state={this.state.editAuthor} /></td>
				    <td><button type="button" onClick={this.delete} className="btn btn-danger btn-sm" data-title="Delete"><i className="fa fa-trash-o"></i></button></td>
				</tr>
			);
	}
});



var AuthorsContainer = React.createClass ({
	getInitialState: function(){
		return {
			data:[],
			filterText: '',
			refresh:false,
			loading: false,
		};
	},
	softRefresh: function(data){
		this.setState({data:data});
	},
	componentWillMount: function() {
		this.refresh();
	},
	refresh: function() {
		$.ajax({
		  url: "/admin/authors/api/get-authors",
		  type: 'POST',
		  beforeSend:function(){
		  	this.setState({refresh:true,loading:true})
		  }.bind(this),
		  success: function(data) {
		  	this.setState({data:data,loading:false})
		  }.bind(this),
		  complete: function(){
		  	this.setState({refresh:false,loading:false})
		  }.bind(this)
		});
	},
	update: function(obj) {
		$.ajax({
	  	  url: "/admin/authors/api/save-author",
	  		data: {
	  			id:obj.id,
	  			name:obj.name,
	  			bio:obj.bio,
	  			article:obj.article
	  		},
	  		type: 'POST',
	  		success: function(data) {
	  			this.setState({data:data})
	  		}.bind(this)
		});
	},
	delete: function(id) {
		$.ajax({
		  url: "/admin/authors/api/delete-author",
		  data: {
		  	id:id
		  },
		  type: 'POST',
		  success: function(data) {
		  	this.setState({data:data})
		  	}.bind(this)
		});
	},
	filterText: function(filterText) {
		this.setState({
			filterText: filterText
		});
	},
	render: function() {
		var loading = '';
		var filterText = this.state.filterText;
		var rows = [];
		var data = this.state.data;

		if(this.state.loading)
			{
				loading = <Loading />;
			}

		var filtered = data.filter(function(data){
			return(data.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1) ? true : false;
		});

		filtered.forEach(function(data,index){
			rows.push(<AuthorsContent update={this.update} delete={this.delete} key={index} data={data} />);
		}.bind(this))
		var spinner = cx({
			"fa fa-refresh": true,
			"fa-spin": !this.state.loaded
		})

		return (
			<div>
				<div className="lasa-page-header">
					<h2 className="pull-left">Authors</h2>
					<div className={'pull-right right-bar '}>
						<SearchBar onUserInput={this.filterText} />
						<button type="submit" className="btn btn-primary btn-lg" onClick={this.refresh} ><i className="fa fa-refresh fa-rotate-90"></i></button>
						<button className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalAdd"><i className="fa fa-plus"></i></button>
						<AddAuthor refresh={this.refresh} softRefresh={this.softRefresh} upload={this.upload} />
					</div>
				</div>
				{loading}
					<div className="row">
						<div className="col-md-12">
			        		<div className={"table-responsive"}>
			        			<table id="mytable" className="table table-hover table-striped">
								<thead>
					            <th>Id</th>
				                <th>Name</th>
				                <th>Bio</th>
								</thead>
								<tbody>
			        			{rows}
			        			</tbody>
								</table>
							</div>
						</div>
					</div>
				<a id="back-to-top" href="#" className="btn btn-primary btn-lg back-to-top" role="button"><i className="fa fa-chevron-up"></i></a>
			</div>
			);
	}
});
	React.renderComponent(<AuthorsContainer />, document.getElementById('content'));