<?php 

class Video extends Eloquent {
	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'magazine_pages_video';
	protected $dates = ['deleted_at'];

}