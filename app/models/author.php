<?php 

class Author extends Eloquent  {

    use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'magazine_article_author';
   	protected $dates = ['deleted_at'];

   	
    
}