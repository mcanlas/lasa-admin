<?php 

class Image extends Eloquent  {

    use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'magazine_images';
   	protected $dates = ['deleted_at'];

   	protected $rootUrl = '/images/';

   	public function getImageArrtribute($value){
   		if(is_null($value)){
   			return 'http://placehold.it/250/250';
   		}
   		return $this->rootUrl.$value;
   	}
}