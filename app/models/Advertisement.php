<?php 

class Advertisement extends Eloquent {
	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'magazine_pages_advert';
	protected $dates = ['deleted_at'];

}