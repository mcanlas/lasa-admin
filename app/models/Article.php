<?php 

class Article extends Eloquent  {

    use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'magazine_articles';
   	protected $dates = ['deleted_at'];
    
}