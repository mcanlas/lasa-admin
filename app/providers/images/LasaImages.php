<?php 

use Config;
use SiteSettings;
use App;
use Input;
use Response;
use User;

class GeUsers {

	public static $table = 'magazine_images';

	public static function getSettings(){
		return Config::get('LasaImages::settings');
	}

	public static function getDescription(){
		return 'test';
	}

	public static function getFromDB(){
		$settings = SiteSettings::whereIn('setting',self::getSettings())->get()->toArray();
		$return = array();
		foreach($settings as $value){
			$return[$value['setting']]=json_decode($value['value']);
		}
		return $return;
	}
	public static function apiCall($type){

		switch ($type) {
			case 'load-columns':
				return self::loadColumns();
				break;
			case 'save':
				return self::saveSettings();
				break;
			default:
				return App::abort(403);
				break;
			}
	}


	public static function loadColumns(){
		$table = self::$table;
		$loaded = $table::first()->toArray();
		return Response::json(array_keys($loaded));
	}

	public static function saveSettings(){
		$input = Input::all();
		foreach($input as $in){
			$setting = SiteSettings::firstOrNew(array('setting' => $in['key']));
			$setting->value = json_encode($in['value']);
			$setting->save();
		}
		return Response::json(Input::all());
	}
}