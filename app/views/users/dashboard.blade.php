@extends('layouts.main')

@section('dashboard')
<link rel="stylesheet" type="text/css" href="/css/dashboard.css" >
<link rel="stylesheet" type="text/css" href="/css/redactor.css" >
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<div id="toggle-open" class="sb-slidebar sb-active" role="navigation">
	<nav>
		<ul class="sb-menu">
			<li class="sb-close">
				<a href="/images/magazine/authors/Noah.jpg" target= "_blank"><img src="/images/logo.png" alt="LASA" width="200" height="70"></a>
			</li>
			<li class="sb-close">
				<a href="/"><i class="fa fa-home"></i> Home</a>
			</li>
			<li class="sb-close">
				<a href="/authors"><i class="fa fa-user"></i> Authors</a>
			</li>
			<li class="sb-close">
				<a href="/articles"><i class="fa fa-file-text"></i> Articles </a>
			</li>
			<li class="sb-close">
				<a href="/magazineImages"><i class="fa fa-picture-o"></i> Images</a>
			</li>
			<li class="sb-close">
				<a href="/pages"><i class="fa fa-book"></i> Pages</a>
			</li>
			<li class="sb-close">
				<a href="/advertisements"><i class="fa fa-usd"></i> Adverts</a>
			</li>
			<li class="sb-close">
				<a href="/videos"><i class="fa fa-video-camera"></i> Videos</a>
			</li>
			<li class="sb-close">
				<a href="/magazines"><i class="fa fa-plus-circle"></i> Magazine</a>
			</li>
		</ul>
	</nav>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="http://fb.me/react-with-addons-0.10.0.js"></script>
<script src="http://fb.me/JSXTransformer-0.10.0.js"></script>
<script type="text/javascript" src="/js/dashboard/dashboard.js"></script>
<script type="text/javascript" src="/js/features/redactor.js"></script>
<script type="text/javascript" src="/js/features/navopen.js"></script>

@stop

