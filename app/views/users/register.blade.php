@extends('layouts.main')

@section('content')
{{ Form::open(array('url'=>'/users/register', 'class'=>'form-signup')) }}
	<h2 class="form-signup-heading">Register</h2>

	<ul> 
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>

	{{ Form::text('firstname', null, array('class'=>'form-control input-sm', 'placeholder'=>'First Name')) }}	
	{{ Form::text('lastname', null, array('class'=>'form-control input-sm', 'placeholder'=>'Last Name')) }}	
	{{ Form::text('email', null, array('class'=>'form-control input-sm', 'placeholder'=>'Email')) }}	
	{{ Form::password('password', array('class'=>'form-control input-sm', 'placeholder'=>'Password')) }}	
	{{ Form::password('password_confirmation', array('class'=>'form-control input-sm', 'placeholder'=>'Confirm Password')) }}	

	{{ Form::submit('Register',array('class'=>'btn btn-large btn-primary btn-block')) }}

{{ Form::close() }}
@stop

