@extends('layouts.main')

@section('content')
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script type="text/javascript" src="/js/login/login.js"></script>
	{{ Form::open(array('url'=>'/users/login', 'class'=>'form-signin')) }}
		<h3 class="form-signin-heading"><i class="fa fa-sign-in"></i> Login</h3>

		{{ Form::text('email', null, array('class'=>'form-control input-sm', 'placeholder'=>'Email Address')) }} <br />
		{{ Form::password('password', array('class'=>'form-control input-sm' , 'placeholder'=>'Password')) }} <br />
		<label>{{ Form::checkbox('remember-me')}} <small>remember me</small></label>

		{{ Form::submit('Login',array('class'=>'btn btn-large btn-primary btn-block ')) }} <br />
		<a href="/users/register" >Register</a>
		

		
	{{ Form::close() }}
	
@stop