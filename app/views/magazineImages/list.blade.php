@extends('users.dashboard')

@section('topbar')
	<ul class="nav navbar-nav pull-right">
	</ul>
@stop

@section('content')
	<div id="content"></div>
@stop

@section('scripts')
	<link rel="stylesheet" type="text/css" href= "/css/magazineImages.css" >
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
	<script  type="text/javascript" src="/js/features/navopen.js"></script>
    <script type="text/javascript" src="/js/features/scroll.js"></script>
    <script type="text/jsx" src="/js/magazineImages/list.js"></script>
@stop