<html lan="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

			<title>LASA Admin</title>
			<link rel="stylesheet" type="text/css" href="/css/dashboard.css" >
			<link rel="stylesheet" type="text/css" href="/packages/bootstrap/css/bootstrap.css">
			<link rel="stylesheet" type="text/css" href="/css/main.css" >
			<link rel="stylesheet" type="text/css" href="/css/redactor.css" >
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
			<script type="text/javascript" src="/js/features/navopen.js"></script>
	</head>
	<body>
		
			<div>
				<ul class="nav navbar-nav">
				@if(!Auth::check())
				
				@else
				<button id="nav-toggle" class="btn btn-link btn-lg pull-left"><i class="fa fa-align-justify"></i> Menu</button>
				<a id="nav-logout" class="pull-right btn btn-link btn-lg " href="/users/logout"><i class="fa fa-sign-out"></i> Logout</a>
				@endif
				</ul>
			</div>
		<div class="container">
			@if(Session::has('message'))
				<p class="alert alert-warning">{{ Session::get('message')}}</p>
			@endif

			
		</div>
		<div id="content" class="content">
			@yield('content')
		</div>
		@yield('scripts')
		@yield('dashboard')
	</body>
</html>

