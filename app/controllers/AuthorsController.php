<?php 

class AuthorsController extends BaseController {

	public function postGetAuthors(){
		return Response::json(Author::whereNull('deleted_at')->get());
	}

	public function postAddAuthor(){
		Log::info(Input::all());
		$author = new Author;

		$author->name=Input::get('name');
		$author->bio=Input::get('bio');
		$author->article=Input::get('article');
		
	$author->save();

	return Response::json(Author::whereNull('deleted_at')->get());
	
	}
	public function postDeleteAuthor(){
		$author = Author::find(Input::get('id'));

		$author->delete();

		return Response::json(Author::whereNull('deleted_at')->get());
	}
	public function postSaveAuthor(){
		$author = Author::find(Input::get('id'));

		$author->name=Input::get('name');
		$author->bio=Input::get('bio');
		$author->article=Input::get('article');

		$author->save();

		return Response::json(Author::whereNull('deleted_at')->get());
	}


}
