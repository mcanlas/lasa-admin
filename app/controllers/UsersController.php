<?php

class UsersController extends BaseController {


	public function anyIndex(){
		return Redirect::to('/');
	}

	public function getLogin() {
		return View::make('users.login');
	}
	public function getRegister() {
		return View::make('users.register');
	}
	public function postRegister() {
			$validator = Validator::make(Input::all(),User::$rules);

			if($validator->passes()) {
				$user = new User;
				$user->firstname = Input::get('firstname');
				$user->lastname = Input::get('lastname');
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->save();

				return Redirect::to('/users/login')->with('message', 'You are now registered');
			} else {
				return Redirect::to('/users/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();

			}

	}

	public function postLogin() {
		if(Auth::check()){
			return Redirect::to('/');
			;
		}
		if(Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')), Input::get('remember-me')))
		{
			return Redirect::intended('/');
		} else {
		return Redirect::to('/users/login')
			->with('message', 'Your username/password combination was incorrect')
			->withInput();

		}
	}

	public function getLogout() {
		Auth::logout();
		return Redirect::to('users/login');
	}
}


