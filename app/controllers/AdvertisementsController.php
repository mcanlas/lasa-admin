<?php 

class AdvertisementsController extends BaseController {

	public function postGetAdvertisements(){
		return Response::json(Advertisement::whereNull('deleted_at')->get());
	}

	public function postAddAdvertisement(){
		Log::info(Input::all());
		$advertisement = new Advertisement;

		$advertisement->type=Input::get('type');
		$advertisement->color=Input::get('color');
		$advertisement->advert_content=Input::get('advert_content');
		$advertisement->advert_left=Input::get('advert_left');
		$advertisement->link=Input::get('link');
		$advertisement->class=Input::get('class');
		$advertisement->logo=Input::get('logo');
		
	$advertisement->save();

	return Response::json(Advertisement::whereNull('deleted_at')->get());
	
	}
	public function postDeleteAdvertisement(){
		$advertisement = Advertisement::find(Input::get('id'));

		$advertisement->delete();

		return Response::json(Advertisement::whereNull('deleted_at')->get());
	}
	public function postSaveAdvertisement(){
		$advertisement = Advertisement::find(Input::get('id'));

		$advertisement->color=Input::get('color');
		$advertisement->type=Input::get('type');
		$advertisement->advert_content=Input::get('advert_content');
		$advertisement->advert_left=Input::get('advert_left');
		$advertisement->link=Input::get('link');
		$advertisement->class=Input::get('class');
		$advertisement->logo=Input::get('logo');

		$advertisement->save();

		return Response::json(Advertisement::whereNull('deleted_at')->get());
	}


}
