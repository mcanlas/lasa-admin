<?php 

class MagazinesController extends BaseController {

	public function postGetMagazines(){
		return Response::json(Magazine::whereNull('deleted_at')->get());
	}

	public function postAddMagazine(){
		Log::info(Input::all());
		$magazine = new Magazine;

		$magazine->key=Input::get('key');

		
	$magazine->save();

	return Response::json(Magazine::whereNull('deleted_at')->get());
	
	}
	public function postDeleteMagazine(){
		$magazine = magazine::find(Input::get('id'));

		$magazine->delete();

		return Response::json(Magazine::whereNull('deleted_at')->get());
	}

}
