<?php 

class ArticlesController extends BaseController {

	public function postGetArticles(){
		return Response::json(article::whereNull('deleted_at')->get());
	}

	public function postAddArticle(){
		Log::info(Input::all());
		$article = new Article;

		$article->name=Input::get('name');
		$article->content=Input::get('content');
		$article->active=Input::get('active');
		$article->color=Input::get('color');
		$article->order=Input::get('order');

	$article->save();

	return Response::json(article::whereNull('deleted_at')->get());
	
	}
	public function postDeleteArticle(){
		$article = Article::find(Input::get('id'));

		$article->delete();

		return Response::json(Article::whereNull('deleted_at')->get());
	}
	public function postSaveArticle(){
		$article = Article::find(Input::get('id'));

		$article->name=Input::get('name');
		$article->content=Input::get('content');
		$article->active=Input::get('active');
		$article->color=Input::get('color');
		$article->order=Input::get('order');

		$article->save();

		return Response::json(Article::whereNull('deleted_at')->get());
	}


}
