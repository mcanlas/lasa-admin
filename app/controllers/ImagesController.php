<?php 

class ImagesController extends BaseController {

	public function postGetImages(){
		return Response::json(Image::whereNull('deleted_at')->get());
	}

	public function postAddImage(){
		Log::info(Input::all());
		$image = new Image;

		$image->position_x=Input::get('position_x','center');
		$image->position_y=Input::get('position_y','center');
		$image->type=Input::get('type','');

		if(Input::hasFile('url')){
			$file = Input::file('url');
			$extension = $file->getClientOriginalExtension();
			$filename = ($file->getClientOriginalName());

			$destinationPath = public_path().'/images/magazine/'.$image->type;

			if($uploadSuccess = $file->move($destinationPath,$filename))
			{
				$image->url = $filename;
			}
		}


		$image->save();

	return Response::json(Image::whereNull('deleted_at')->get());
	
	}
	public function postDeleteImage(){
		$image = Image::find(Input::get('id'));

		$image->delete();

		return Response::json(Image::whereNull('deleted_at')->get());
	}
	public function postSaveImage(){
		$image = Image::find(Input::get('id'));

		$image->position_x=Input::get('position_x');
		$image->position_y=Input::get('position_y');
		$image->type=Input::get('type');


		$image->save();

		return Response::json(Image::whereNull('deleted_at')->get());
	}

}
