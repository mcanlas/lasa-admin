<?php 

class VideosController extends BaseController {

	public function postGetVideos(){
		return Response::json(Video::whereNull('deleted_at')->get());
	}

	public function postAddVideo(){
		Log::info(Input::all());
		$video = new Video;

		$video->color=Input::get('color');
		$video->class=Input::get('class');
		$video->video=Input::get('video');
		$video->title=Input::get('title');
		$video->text=Input::get('text');
		$video->link=Input::get('link');

	$video->save();

	return Response::json(Video::whereNull('deleted_at')->get());
	
	}
	public function postDeleteVideo(){
		$video = Video::find(Input::get('id'));

		$video->delete();

		return Response::json(Video::whereNull('deleted_at')->get());
	}
	public function postSaveVideo(){
		$video = Video::find(Input::get('id'));

		$video->color=Input::get('color');
		$video->class=Input::get('class');
		$video->video=Input::get('video');
		$video->title=Input::get('title');
		$video->text=Input::get('text');
		$video->link=Input::get('link');

		$video->save();

		return Response::json(Video::whereNull('deleted_at')->get());
	}

}
