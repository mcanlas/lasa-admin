<?php 

class PagesController extends BaseController {

	public function postGetPages(){
		return Response::json(Page::whereNull('deleted_at')->get());
	}

	public function postAddPage(){
		Log::info(Input::all());
		$page = new Page;

		$page->type=Input::get('type');
		$page->color=Input::get('color');
		$page->alias=Input::get('alias');
		$page->name=Input::get('name');
		$page->ref=Input::get('ref');
		$page->order=Input::get('order');
		$page->magazine=Input::get('magazine');

		$page->save();

		return Response::json(Page::whreNull('deleted_at')->get());

	}

	public function postDeletePage(){
		$page = Page::find(Input::get('id'));

		$page->delete();

		return Response::json(Page::whereNull('deleted_at')->get());
	}
	public function postSavePage(){
		$page = Page::find(Input::get('id'));

		$page->type=Input::get('type');
		$page->color=Input::get('color');
		$page->alias=Input::get('alias');
		$page->name=Input::get('name');
		$page->ref=Input::get('ref');
		$page->order=Input::get('order');
		$page->magazine=Input::get('magazine');

		$page->save();

		return Response::json(Page::whereNull('deleted_at')->get());
	}

}