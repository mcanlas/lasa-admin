<?php

Route::controller('users', 'UsersController');

Route::get('/users/logout',function() {
	Auth::logout();
});
Route::controller('authors', 'AuthorsController');
Route::controller('magazineImages', 'ImagesController');
Route::controller('pages', 'PagesController');
Route::controller('advertisements', 'AdvertisementsController');
Route::controller('videos', 'VideosController');
Route::controller('articles', 'ArticlesController');
Route::controller('magazines', 'MagazinesController');

Route::group(array('after' => 'auth'), function()
{
	Route::get('/', function(){
		return View::make('users.dashboard');
	});

	Route::get('authors', function(){
		return View::make('authors.list');
	});

	Route::get('articles', function()
	{
		return View::make('articles.list');
	});

	Route::get('magazineImages', function()
	{
		return View::make('magazineImages.list');
	});

	Route::get('pages', function()
	{
		return View::make('pages.list');
	});

	Route::get('advertisements', function()
	{
		return View::make('advertisements.list');
	});

	Route::get('videos', function()
	{
		return View::make('videos.list');
	});

	Route::get('magazines', function()
	{
		return View::make('magazines.list');
	});

});

Route::group(array('after'=>'auth','prefix' => '/admin/authors'), function()
{
	Route::get('authors',function()
	{
		return View::make('LasaAuthors::list');
	});

	Route::controller('/api','\AuthorsController');
});

Route::group(array('after'=>'auth','prefix' => '/admin/pages'), function()
{
	Route::get('pages',function()
	{
		return View::make('LasaPages::list');
	});

	Route::controller('/api','\PagesController');
});


Route::group(array('after'=>'auth','prefix' => '/admin/advertisements'), function()
{
	Route::get('advertisements',function()
	{
		return View::make('LasaAdvertisements::list');
	});

	Route::controller('/api','\AdvertisementsController');
});

Route::group(array('after'=>'auth','prefix' => '/admin/videos'), function()
{
	Route::get('videos',function()
	{
		return View::make('LasaVideos::list');
	});

	Route::controller('/api','\VideosController');
});


Route::group(array('after'=>'auth','prefix' => '/admin/articles'), function()
{
	Route::get('articles',function()
	{
		return View::make('LasaArticles::list');
	});

	Route::controller('/api','\ArticlesController');
});

Route::group(array('after'=>'auth','prefix' => '/admin/magazines'), function()
{
	Route::get('magazines',function()
	{
		return View::make('LasaMagazines::list');
	});

	Route::controller('/api','\MagazinesController');
});

Route::group(array('after'=>'auth','prefix' => '/admin/images'), function()
{
	Route::get('images',function()
	{
		return View::make('LasaImages::list');
	});

	Route::controller('/api','\ImagesController');
});